#!/usr/bin/python3
import pyaudio
import numpy as np
import sys
import sdl2
import sdl2.ext
import time

BLACK = sdl2.ext.Color(0, 0, 0)
WHITE = sdl2.ext.Color(255, 255, 255)
GRAY = sdl2.ext.Color(144, 154, 144)
GREEN= sdl2.ext.Color(0, 255, 0)
DGREEN= sdl2.ext.Color(0, 255, 0, 255)
render_scale = 2

def options(error=""):
    print(error + """Options: 
    -x\t\tFlip x-axis
    -y\t\tFlip y-axis
    -l\t\tConnect dots to lines
    -z factor\t\tRender zoom (better to use volume)
    -w size\t\tWindow size in pixels
    -r rate\t\tAudio sample rate
    -p 0-255\t\tImage persistence/Burn-in simulation 255-no persistance 
    -f 0-255\t\tFade in to hide sampling-related artifacts 255-full alpha drawn""")
    quit()

def soundplot(stream, r, uselines):
    data = ((np.fromstring(stream.read(chunk_size),dtype=np.int16) * scaleval) + coffs).astype(int)
    if(uselines):
        r.draw_line(data,color=DGREEN)
    else:
        r.draw_point(data,color=DGREEN)

def run():
    samp_rate = 192000 #96000 
    windowsize = 800
    zoom = 1 
    maxrecip=1/32768

    flipx, flipy = False,False
    uselines = False
    running = False

    draw_alpha = 36 # Fade in to hide sampling-related artifacts  255=full alpha drawn
    alpha_filter = 45 # 0-255 Image persistence/Burn-in simulation. 255=no persistence

    if(len(sys.argv) > 1):
        ai = iter(sys.argv)
        for a in ai:
            try:
                flipx = (a == "-x") or flipx
                flipy = (a == "-y") or flipy
                uselines = (a == "-l") or uselines
                if (a == "-z"):
                    zoom = float(next(ai)) 
                if (a == "-w"):
                    windowsize = int(next(ai))
                if (a == "-r"):
                    samp_rate = int(next(ai))
                if (a == "-p"):
                    alpha_filter = 255 - int(next(ai))
                    if (alpha_filter < 0 or alpha_filter > 255):
                        raise ValueError('Bad value')
                if (a == "-f"):
                    draw_alpha = 255 - int(next(ai))
                    if (draw_alpha < 0 or draw_alpha > 255):
                        raise ValueError('Bad value')
            except ValueError:
                options("Bad value!")
            except StopIteration:
                options("Value missing.")
                
    
    global DGREEN
    DGREEN=sdl2.ext.Color(0, 255, 0, draw_alpha)

    global DARKEN
    DARKEN = sdl2.ext.Color(0, 0, 0,alpha_filter)

    global chunk_size
    chunk_size = int(samp_rate/30)              # samp_rate / number of updates per second

    global flip, scaleval, coffs
    sze = chunk_size * 2
    coffs = np.ones(sze) * (windowsize/(2*render_scale))
    flip = np.ones(sze).astype(int)
    flip[1::2] = -1
    if flipy:
        flip[1::2] = 1
    if flipx:
        flip[0::2] = -1

    scaleval = zoom * (maxrecip*(windowsize/(2*render_scale))) * flip

    p = pyaudio.PyAudio()
    
    sdl2.ext.init()
    window = sdl2.ext.Window("Plot", size=(windowsize, windowsize))
    rndr = sdl2.ext.Renderer(window, flags=sdl2.render.SDL_RENDERER_ACCELERATED)

  
    rndr.scale=(render_scale,render_scale)
    rndr.blendmode = sdl2.SDL_BLENDMODE_BLEND 

    running = True
    window.show()
    rndr.fill(BLACK)
    rndr.present()

    stream=p.open(format=pyaudio.paInt16, channels=2, rate=samp_rate, input=True,
            frames_per_buffer=chunk_size)

    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
                break
            if event.type == sdl2.SDL_KEYDOWN:
                if event.key.keysym.sym == sdl2.SDLK_UP:
                    alpha_filter += 1 
                elif event.key.keysym.sym == sdl2.SDLK_DOWN:
                    alpha_filter -= 1 
                if event.key.keysym.sym == sdl2.SDLK_LEFT:
                    draw_alpha -= 1 
                elif event.key.keysym.sym == sdl2.SDLK_RIGHT:
                    draw_alpha += 1 
                draw_alpha = 255 if draw_alpha > 255 else (0 if draw_alpha < 0 else draw_alpha)
                alpha_filter = 255 if alpha_filter > 255 else (0 if alpha_filter < 0 else alpha_filter)
                DGREEN=sdl2.ext.Color(0, 255, 0, draw_alpha)
                DARKEN = sdl2.ext.Color(0, 0, 0,alpha_filter)
                print("Draw alpha/fade-in: %s, Filter opacity/persistence: %s" % (draw_alpha, alpha_filter))
        rndr.fill((0,0,windowsize,windowsize),color=DARKEN)
            
        soundplot(stream, rndr, uselines)
        rndr.present()
    
    stream.stop_stream()
    stream.close()
    window.close()
    quit()
