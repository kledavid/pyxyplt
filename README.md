# Python Audio X-Y-Oscilloscope

# Installation:

`python3 setup.py install`

# Usage:

`python3 -m py_xyplot [options]`

# Options: 
    -x		Flip x-axis
    -y		Flip y-axis
    -l		Connect dots to lines
    -z factor		Render zoom (better to use volume)
    -w size		Window size in pixels
    -r rate		Audio sample rate
    -p 0-255		Image persistence/Burn-in simulation 255-no persistance 
    -f 0-255		Fade in to hide sampling-related artifacts 255-full alpha drawn

