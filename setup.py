import os
from setuptools import setup

requirements = [
    "pyaudio",
    "pygame",
    "numpy",
    "PySDL2",
]

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def readme():
    return open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

setup(
    name = "py_xyplot",
    version = "1.0",
    install_requires=requirements,
    author = "David Kleymann",
    author_email = "david.kleymann@posteo.de",
    description = ("An Audio X-Y-Plot Oscilloscope"),
    license = "BSD",
    keywords = "audio oscilloscope xy plot music",
    packages=['py_xyplot'],
    long_description=readme(),
) 
